# Modul 143, Backup

- **Intro:**

   - Mit einem Backup zu tun haben ist etwas vom zentralsten in der Informatik. Ohne Backup wären alle Daten völlig unsicher, denn wenn sie 
     einmal verloren gehen sind sie weg. Deshalb ist es wichtig zu wissen wie man ein Backup vernünftig erstellt und auch die kontrolle darüber hat.

   - In diesem Dokument finden Sie die Dokumentation über meine komplette Umgebung mit: TrueNas, Backup, Windows 2019 Server und Windows 10 Client.



## Inhaltsvezeichnis
    - Netzplan
    - Aufsetzung des Windows 2019 Server
    - Aufsetzung des Windows 10 Clients
    - Aufsetzung des TrueNas
    - Hinzufügung des Backups
    - Berechtigungen, Gruppen und User
    - Testfälle
    - Kosten
    - Auswertung

## Dokumentation


### Netzplan:

In diesem Link finden Sie meinen erstellten Netzplan: [Netzplan](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/Netzplan.png)


Zusätzlich zum Netzplan habe ich folgende Services benutzt:
   - Active Directory Services (DNS, File System, User and Groups, Event Viewer usw.)
   - SMB (für TrueNas zum teilen von Speicher)
   - PRTG (zur Überwachung)
   - Windows Backup Service

### Aufsetzung des Windows 2019 Server:

- **Allgemeines:** Zuerst habe ich begonnen den Windows Server aufzusetzen. Das habe ich aus den logischen Gründen gemacht, dass ich einen DNS Server zur verfügung habe, und um alle Geräte in eine Domäne zu setzten. 

- **Ressourcen:** Ich habe dem Windows Server 4096 MB RAM gegeben und 2 CPUs. Die Version des Servers ist diese aus dem Jahr 2019.
                  Dem Server habe ich eine dynamisch allozierte Festplatte mit 60.56 GB gegeben.

- **Konfiguration:** Als erstes habe ich den Server folgend konfiguriert: [IP vergebung](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/windows.server.ip.setzung.png)
                     
                     Danach habe ich die Rolle Active Directory Domain Services und den DNS Server installiert.
                     Als diese Installationen fertig waren habe ich die Domain "lino.local" hinzugefügt.
                     Beim DNS Server habe musste ich noch eine Reverse Lookup Zone hinzufügen: 
[Reverse Lookup Zone](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/windows.server.dns.png)
                     





### Aufsetzung des Windows 10 Clients:

- **Allgemeines:** Der Windows 10 Client dient als Gerät um Tests durchzuführen.

- **Ressourcen:** Ich habe dem Windows 10 Client 2048 MB RAM gegeben und 2 CPUs. Der Client hat eine Umgebung mit Windows 10.
                  Dem Client habe ich eine dynamisch allozierte Festplatte mit 60 GB gegeben

- **Konfiguration:** Dem Client habe ich folgende Konfiguration hinzugefügt: 
                     IP: 192.168.10.100, DNS: 192.168.10.1
                     Logischwerweise habe ich den Client in das gleiche Netz wie der Server gesetzt.


                     Danach habe ich die Verbindung zwischen Server und Client getestet, welche auch 
                     funktionierte.
                     Somit habe ich den Client in die Domäne "lino.local" eingebunden




                     

                     


### Aufsetzung des TrueNas:

- **Allgemeines:** Das TrueNas dient dazu dass alle Geräte im Netzwerk auf die, auf dem TreuNas gespeicherten Daten zuzugreifen und natürlich auch 
                   selber abzulegen.


- **Ressourcen:** Ich habe dem TrueNas 8192 MB RAM gegeben und 2 CPUs.
                  Dem TrueNas habe ich eine dynamisch allozierte Festplatte mit 60 GB gegeben.

- **Konfiguration:** Das TrueNas habe ich folgender Massen konfiguriert: [TrueNas Konfiguration](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/truenas.konfiguration.png)
                     
                     Somit konnte ich über den Client mit der Ip 192.168.10.5 über den Browser auf das Nas 
                     zugreifen und die wichtigsten Konfigurationen tätigen. 
                     
[Zugriff über Browser](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/truenas.konfiguration.mit.client.png)

                     Als erstes habe ich alles erstellet und konfiguriert, was für den Zugriff vom Netzwerk auf 
                     den Speicher nötig ist.
                     
                     Folgendes habe ich dabei erstellt und konfiguriert:
                     1. Einen Pool erstellt 

[Pool erstellung](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/truenas.konfiguration.pool.png)

                     2. Meine gewünschten Disk auswählen auf welchen die Daten gespeichert werden sollten. 
                        (Danach ist der Pool erstellt.)

                     3. Danach habe ich einen User estellt. Der User heisst lino1

                     4. Add Dataset und einen Namen dem Dataset geben. Ich habe den Namen Datenbank 1 gegeben.
                        Zusätzlich habe ich die berechtigungen logisch verteilt.

[Dataset](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/ScreensTreuNas.add.dataset.png)

                     5. Danach habe ich bei "Windows Shares" den Share erstellt, sodass ich auch vom Clietn auf 
                        den Speicher zugreiffen kann.
                        Beim erstellen habe ich zuerst den Pfad von meinem Dataset angegeben. (Wichtig: SMB muss 
                        aktiviert sein)

                     6. Danach muss man ACL Konfigurieren 

[ACL Konfiguration](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/ScreensTreuNas.Windows.Sharing.png)

                     7. Auf dem Client ein Netzwerk Laufwerk einbinden. Ich habe den Buchstaben L vergeben. 

[Laufwerk einbinden](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/ScreensTreuNas.Netwerklaufwer.Einbinden.png)

[Eingebundenes Laufwerk](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/ScreensTreuNas.Netwerklaufwer.png) 
                        
                     8. Und somit kann ich jetzt vom Client auf den Speicher des Truenas zugreiffen.

            
                     
                                   
                     
                     
                     

### Hinzufügung des Backups:
   - **Allgemeines:** Ich habe für den Backup Service den Windows 2019 Server ausgewählt.
                      Die genauen Details zum Server finden Sie oben im Kapitel Aufsetzung des Windows 2019 Server.
                      Die gesicherten Daten speichere ich auf dem Server selber ab.

   - **Konfiguration:** Ich habe auf dem Windows Server das feature Backup Service installiert.
                        Danach habe ich mit den Grundkonfigurationen begonnen.

                        
                        Optimize Backup Performance: 
                           - Ich habe die Einstellung Faster backup performance ausgewählt, was nichts anderes als ein Inkrementelles Backup 
                             bedeutet. 
                           - Ich habe ein Inkrementelles Backup gewählt, da man zuerst ein ganzes Backup macht und danch alle änderungen 
                             immer wieder gesichert werden. Was bedeutet, dass der Umfang der Datensicherung deutlich geringer ist als bei anderen
                             Datensicherungs Möglichkeiten. Und weniger Speicherplatz verbrauch bedeutet weniger Kosten.
[Inkrementelles Backup](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/windows.backup.konfiguration.inkrementell.backup.png)
                        
                        Backup Schedule:
                           - Als erstes habe ich das gewünschte Laufwerk das gesichert werden soll ausgewählt, natürlich das geteilte Laufwerk "L"
                             (vom TrueNas).
                           - Danach habe ich die genaue Zeit eingestellt, wann überhaupt die einzelnen Backups stattfinden werden.
                             Ich habe die Zeit 23:00 Uhr ausgewählt. Somit werden jede ändernungen die an diesem Tag getätig wurden, um dieses Zeit 
                             automatisch gesichert. Somit kann ich immer änderungen vornehmen ohne die Angst zu haben, dass ich ein Backup vergesse 
                             zu machen. Dazu ist das Backup in der Nacht (um 23 Uhr) was der Vorteil ist, dass man um diese Zeit nicht am Arbeiten 
                             ist und es somit kein Konflikt zwischen User und Backup gibt.
                           
[Backup Zeitpunkt](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/windows.backup.konfiguratin.ein.mal.pro.Tag.um.2300.uhr.png)

                        Kontrolle:
                           - Um zu kontrollieren ob das Backup wirklich auch um die richtige Zeit und erfolgreich durchgeführt wurde kann man bei den
                             Logs ablesen.
                           - Das geht folgenderweise: Im Server unter Tools, auf Event Viewer, dann auf Applications ans Services, dann Microsoft,
                             danach Windows und zum Schluss Backup auswählen.
                           - Oder man kann folgendes tun: Win + R drücken und %windir%\System32\winevt\Logs\ eingeben.
                             Danach kann man nach dem Backup suchen.
                           - Falls es ein Problem mit dem Backup geben sollte, habe ich eingestellt, dass es einen Filter im Event Viewer hat, der 
                             unter Windows Logs, bei Application ist. Wichtig ist das man beim diesem Filter die Event-IDs 14, 5, 1 auswählt, da
                             diese für die Backup Logs verantwortlich sind.
[Backup Probleme](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/Backup.Fehler.Logs.png)

                        Speicherverwaltung:
                           - Das Backup wird auf der Local Disk C gesichert, der Pfad ist: C:\Backup\...
                           - Man sollte dem Server mehr als genug Speicherplatz vergeben, am besten
                             doppelt so viel wie man braucht, da der Speicher 
                             sich schneller füllt als gedacht. 
                           - Im Notfall kann man auch noch mehr Speicher kaufen.
                           - Speicheroptimierung: Um zu sehen wann man zu wenig Speicher hat sollte man 
                             PRTG als Systemüberwachung benutzen. Man kann sich ein E-Mail zukommen lassen
                             und kann sofort änderungen machen. Man kann auch frühe Warnungen sich 
                             zukommen lassen. 
                             
                        Die eigentliche Systemwiederherstellung:
                           - Um nicht nur die Benutzerdaten zu sichern sondern auch das System selber kann 
                             man die Virtuellen Maschinen die ich erstellt habe, exportieren und auch 
                             abspeichern. Und natürlich darüber auch ein Backup machen. Einerseits würde 
                             ich in meinem System ein Backup machen und auch noch in einem anderen System.
                           - Dieser vorgang ist sehr wichtig, denn wenn das System verloren geht ist es 
                             nicht besonders lustig, ohne Backup!
                           
                        

                        

### Berechtigungen, Gruppen, User

   - Gruppen: loc_Administration, loc_Normale_Mitarbeiter, loc_IT_Help
   - User: 1 Root_User, 1 Backup_User, 20 Normale_User, 3 IT_Help_User 
   - Usernamen vergebung: Der Name des Mitarbeiter + Die Art des Mitarbeiter (z.B Normale_User)
   - Der Root_User und der Backup_User sind in der Gruppe Administration, die restlichen User sind in der 
     Gruppe, passend zum Namen.




- r = read
- w = write
- e = execute

- Wichtig!: Auf den Backups dieser Daten gilt die gleiche Struktur bei dem Speicherort, einfach auf der Disk C:\.

- Nur die Gruppe Administration hat berechtigungen bei den Backups (C:\Backup\...).


### Testfälle:
   


   - **Testnummer:** 1


      - Beschreibung: Ich möchte Testen ob die Backups die ich machen in den Log Files sichtbar sind.
                 

      - Voraussetzung: Der Backup Server muss konfiguriert sein und ich muss ein Backup starten oder 
                       allgemein etwas, was mit dem Backup zu tun hat.


      - Wo und wie genau wird getestet: Auf dem Backup Server. In den Backup Einstellungen möchte ich 
        Einstellungen vornhemen.
                                   

      - Testobjekt: Backup Server und Log Files


      - Konsequenzen bei Nichterfüllung: Ich habe dann keine gute Kontrolle über die Backups die 
        ausgeführt wurden oder eben nicht ausgeführt wurden!


      - Zu erwartendes Ergebnis: Ich erwarte, dass ich meine Taten im Backup bereich, im Event Viewer bei 
                                 den Log Files sehe
   

      - Ergebnis: Es hat funktioniert, ich sehe meine veränderungen

[Test 1 Ergebnis](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/Backup.Log.Kontrolle.png)

   - **Testnummer:** 2

      - Beschreibung: Ich möchte testen, ob das komplette Backup das ich am anfang erstellt habe funktioniert. (Das erste Voll-Backup beim Inkrementellen Backup )

      - Voraussetzung: Es wird ein Backup Service benötigt.

      - Wo und wie genau wird getestet: Ich lade Daten auf das TrueNas, danach mache ich ein Backup auf diese Daten. Nachher lösche ich die 
                                        Daten und schaue ob ich die Daten wiederherstellen kann.

      - Testobjekt: Backup Server

      - Konsequenzen bei Nichterfüllung: Wenn die Daten verloren gehen, komme ich nie mehr zu diesen Daten zurück.

      - Zu erwartendes Ergebnis: Ich erwarte das ich die Daten wiederherstellen kann.

      - Ergebnis: Das Backup feature hat funktioniert, die Daten sind wieder da!

[Test 2 Ergebnis](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/Backup.Test2.png)
   

   - **Testnummer:** 3

      - Beschreibung: Ich möchte Testen ob ich über den Browser auf das TrueNas zugreiffen kann, um 
       jederzeit wichtige änderungen auf meinem TrueNas zu machen.
      
      - Voraussetzung: TrueNas ist konfiguriert und Server, Client und TrueNas sind im gleichen Netz.

      -  Wo und wie genau wird getestet: Ich gehe beim Client auf dne Browser und gebe die IP des TrueNas 
         ein (192.168.10.5).

      - Testobjekt: TrueNas 

      - Konsequenzen bei Nichterfüllung: Falls ich wichtige änderungen wie mehr Speicher freigeben machen
        muss, kann ich das nicht einfach über den Browser machen. Auch sonstige wichtige konfigurationen 
        kann ich nicht über den Browser machen. (Auch wenn man in der TrueNas VM selber auch Änderungen 
        machen kann, ist es viel übersichtlicher und einfacher im Browser!)
      - Zu erwartendes Ergebnis: Ich erwarte, dass ich über den Browser auf das TrueNas zugreiffen kann und
        wichtige Konfigurationen vornehmen kann.

      - Ergebnis: Es hat funktioniert, ich konnt Zugreiffen (über den Browser)

[Test 3 Ergebnis](https://gitlab.com/LinoBaserga/m143-backup/-/blob/main/truenas.konfiguration.mit.client.png)




                             

### Kosten:
   
   - **Umgebung:**
      - Diese Kosten sind für ein Unternehmen mit mehreren Angestellten. Deshalb hat der Server eine ziemlich gute Leistung.

   - **Lizenzen:**

      - Windows Server 2019 Lizenzen = 1499,90 Fr / Laufzeit: Unbegrenzt
      - TrueNas: OpenSource / Laufzeit: Unbegrenzt
   
   - **Server und Storage:**

      - Server (Dell PowerEdge R450 Silver 4314) = 3102 Fr 
      - CPU  (Intel Xeon Silver 4314) = 842 Fr 
      - RAM (4x) (Kingston Server-Memory KTD-PE432 (je 64 GB Ram)) = 1596 Fr 
      - Storage (+ 16 TB)  (Synology DS920+) = 969 Fr
      - Server  Gehäuse (mit Mainboard…) (5 Supermicro 6029P-TR) = 1910 Fr  
      - SSD (3 x) Gigabyte Aorus Gen4 AIC (je 8 Terabyte) = 5448 Fr 

   - **Energieverbrauch:**
      - Ein Server verbraucht pro Arbeitstag ungefähr 10 kWh
      - 10 kWh = Das sind ungefähr 2.70 Fr. Somit kostet das pro Monat 81 Fr (ungefähr)
      - Ich zähle als kosten nur den Server, sonstige Geräte die im Sytem gebraucht werden nicht (z.B PCs)
      

   - **Gesamtkosten:**
      - 15'366.90 Fr
      - Ungefähr 81 Fr pro Monat für Stromkosten.
      

### Auswertung
   
   Ich habe die Arbeit des Modul 143 eine zentrale und wichtige Arbeit gefunden. Ich habe viele neues Informatik Wissen gelernt, welches ich sicher 
   auch in anderen Modulen brauchen kann. Die Praktische Arbeit fand ich gut zu machen, während sich die Dokumentation eher in die länge gezogen hat.
   Schlussendlich kann man sagen das es ein hilfreiches und auch spannendes Modul ist.



